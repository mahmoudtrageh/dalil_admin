<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <title>@yield('title')</title>
    @include('site.assets.Site-CSS')
    @yield('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<body>

<!-- Start Header-->
<div class="header">
    <div class="container">
        <div class="row">

            <div class="col-md-1">
            <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow navbar-semi-light bg-cyan bg-darken-3 navbar-shadow">
                <div class="navbar-wrapper">
                    <div class="navbar-container content">
                        <div class="collapse navbar-collapse" id="navbar-mobile">
                            <ul class="nav navbar-nav float-right">
                                <li class="dropdown dropdown-user nav-item">

                                    @if(auth()->user())

                                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                            <i style="position: absolute;left: 30px;" class="fa fa-user mr-5"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">

                                            <a class="dropdown-item" href="{{route('site.profile.index',['id'=>auth()->user()->id])}}"><i class="ft-user"></i> Edit Profile</a>

                                            <div class="dropdown-divider"></div>

                                            <a class="dropdown-item" href="{{route('site.logout')}}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{route('site.logout')}}" method="get" style="display: none;">
                                                {{csrf_field()}}
                                            </form>
                                        </div>


                                    @else
                                        <a class="login" href="{{route('site.get.login')}}"><i class="fa fa-sign-in"></i>Login</a>

                                    @endif

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            </div>
            <div class="col-md-3">
                <div class="icons">
                    <div class="icon-1"><i class="fa fa-facebook"></i></div>
                    <div class="icon-2"><i class="fa fa-twitter"></i></div>
                    <div class="icon-3"><i class="fa fa-youtube"></i></div>
                    <div class="icon-4"><i class="fa fa-instagram"></i></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="logo">
                    <a href="{{route('site.index')}}"><img src="{{asset('site/img/05.png')}}" width="100px" height="100px"></a>
                </div>
            </div>
            <div class="col-md-4 email-center">
                <div class="email">
                    <p class="lead">Email: info@spectraapps.com</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Header-->

@include('errors.errors')

<div class="app-content content mt-3">
@yield('content')
</div>

<!--Satrt Footer-->
<div class="footer text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="logo">
                    <img src="{{asset('site/img/05.png')}}" width="200px" height="70px">
                </div>
            </div>
            <div class="col-md-6">
                <div class="store">
                    <img src="{{asset('site/img/app-store.jpg')}}" width="200px" height="50px">
                    <img src="{{asset('site/img/google-play.png')}}" width="200px" height="50px">
                </div>
            </div>
        </div>
    </div>
</div>
<!--End Footer-->

@include('site.assets.Site-JS')
@yield('scripts')

</body>

</html>
