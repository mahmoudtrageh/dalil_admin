@extends('site.layouts.Master-Layout')

@section('title')

    تواصل معنا

@stop

@section('content')

    <!--Contact form start-->
    <div class="contact-form ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section-title text-center">
                        <h2 id="contact" class="black">تواصل معنا</h2>
                    </div>
                </div>
            </div>
            @include('errors.errors')
            <div class="row">

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="contact-form">
                        <p class="form-messege"></p>
                        <form id="contact-form" action="{{route('site.contact.us')}}#contact" method="post">
                            {{csrf_field()}}
                            <input class="form-control mb-2" type="text" name="name" value="{{old('name')}}" placeholder="الاسم">
                            <input class="form-control mb-2" type="email" name="email" value="{{old('email')}}" placeholder="البريد الالكتروني">
                            <input class="form-control mb-2" type="number"  name="phone" value="{{old('phone')}}" placeholder="رقم التليفون">
                            <textarea class="form-control mb-2" name="message"  placeholder="الرسالة">{{old('message')}}</textarea>
                            <button class="btn btn-primary mb-5" type="submit">ارسال</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Contact form end-->


@stop