@extends('site.layouts.Master-Layout')

@section('title')

    الرئيسيه

@stop

@section('content')
    <!-- Start Slider-->

    <div id="slider" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{asset('site/img/01.jpg')}}" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{asset('site/img/02.jpg')}}" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{asset('site/img/03.jpg')}}" height="539.59" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="{{asset('site/img/04.jpg')}}" height="539.59" class="d-block w-100" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- End Slider-->

    <!-- Start Select-->
    <form method="get" action="{{route('site.company.filter')}}">
    <div class="select">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <select name="country" class="country form-control custom-select">
                        <option disabled selected>اختر
                            الدولة
                        </option>

                        @foreach($countries as $country)

                            <option
                                    value="{{$country->id}}">{{$country->arabic_name}}
                            </option>

                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="mohafza" class="mohafza form-control custom-select">
                        <option selected disabled>  اختر الدولة أولًا</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="city" class="city form-control custom-select">
                        <option selected disabled>  اختر المحافظة أولًا</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="section" class="section form-control custom-select">
                        <option selected disabled>  اختر القطاع</option>
                        @foreach($sections as $section)

                            <option
                                    value="{{$section->id}}">{{$section->arabic_name}}
                            </option>

                        @endforeach
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="tasnif" class="tasnif form-control custom-select">
                        <option selected disabled>  اختر القطاع أولًا</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="zone" class="zone form-control custom-select">
                        <option selected disabled>  اختر المدينة أولًا</option>
                    </select>
                </div>
                <div class="col-md-3">
                        <div class="input-group flex-nowrap">
                            <div class="input-group-prepend">
                            </div>
                            <input type="text" name="code" class="form-control" placeholder="الكود" aria-label="code" aria-describedby="addon-wrapping">
                        </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group flex-nowrap">
                        <div class="input-group-prepend">
                        </div>
                        <input type="text" name="name" class="form-control" placeholder="اسم الشركة" aria-label="company-name" aria-describedby="addon-wrapping">
                    </div>
                </div>

                <div class="search">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>بحث</button>
                </div>
            </div>
        </div>
    </div>
    </form>


    <!-- End Select-->

    <!-- Start Data Table-->

        <div class="container table-responsive" style="overflow-x: scroll;">
            <table id="example" class="table table-striped table-bordered" style="width:100%;">
                <thead>
                <tr>
                    <th>اسم الشركة</th>
                    <th>الكود</th>
                    <th>الدولة</th>
                    <th>المحافظة</th>
                    <th>المدينة</th>
                    <th>المنطقة</th>
                    <th>القطاع</th>
                    <th>التصنيف</th>
                    <th>العنوان</th>
                    <th>رقم التليفون</th>
                    <th>اسم المسؤول</th>
                    <th>رقم الموبايل</th>
                    <th>رقم الواتس</th>
                    <th>الايميل</th>
                </tr>
                </thead>
                <tbody>

                <tr>

                    @if($companies)
                        @foreach($companies as $company)

                            <td>{{$company->name}}</td>
                            <td>{{$company->code}}</td>
                            <td> {{$company->country->arabic_name}}</td>
                            <td> {{$company->mohafza->arabic_name}}</td>
                            <td>{{$company->city->arabic_name}}</td>
                            <td>{{$company->zone->arabic_name}}</td>
                            <td>{{$company->section->arabic_name}}</td>
                            <td>{{$company->tasnif->arabic_name}}</td>
                            <td>{{$company->address}}</td>
                            <td>{{$company->phone_number}}</td>
                            <td>{{$company->res_name}}</td>
                            <td>{{$company->mobile_number}}</td>
                            <td>{{$company->whatsapp_number}}</td>
                            <td>{{$company->email}}</td>
                </tr>

                @endforeach
                @endif

                </tbody>
            </table>
        </div>

    <!-- End Data Table-->
@stop

@section('scripts')


    <script>
        $(document).on("change", ".country", function (e) {
            e.preventDefault();
            var id = $(this).val();
            var token = "{{ csrf_token() }}";

            $.ajax({
                url: "{{ route('site.get.mohafza') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    $('.mohafza').html('<option selected disabled> اختر المحافظة</option>');
                    data.user.forEach(function (val){
                        $(".mohafza").append('<option value="' + val.id + '">' + val.arabic_name + '</option>');
                    });

                    if (data.status !== "ok") {
                        alert("ERROR");
                    }

                },
                error: function () {
                    alert("ERROR");
                }
            })
        })


        $(document).on("change", ".mohafza", function (e) {
            e.preventDefault();
            var id = $(this).val();
            var token = "{{ csrf_token() }}";

            $.ajax({
                url: "{{ route('site.get.city') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    $('.city').html('<option selected disabled> اختر المدينة</option>');
                    console.log(data.user);
                    data.user.forEach(function (val){
                        $(".city").append('<option value="' + val.id + '">' + val.arabic_name + '</option>');

                    });

                    if (data.status !== "ok") {
                        alert("ERROR");
                    }

                },
                error: function () {
                    alert("ERROR");
                }
            })
        })

        $(document).on("change", ".city", function (e) {
            e.preventDefault();
            var id = $(this).val();
            var token = "{{ csrf_token() }}";

            $.ajax({
                url: "{{ route('site.get.zone') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    $('.zone').html('<option selected disabled> اختر المنطقة</option>');
                    console.log(data.user);
                    data.user.forEach(function (val){
                        $(".zone").append('<option value="' + val.id + '">' + val.arabic_name + '</option>');

                    });

                    if (data.status !== "ok") {
                        alert("ERROR");
                    }

                },
                error: function () {
                    alert("ERROR");
                }
            })
        })

        $(document).on("change", ".section", function (e) {
            e.preventDefault();
            var id = $(this).val();
            var token = "{{ csrf_token() }}";

            $.ajax({
                url: "{{ route('site.get.tasnif') }}",
                type: "post",
                dataType: "json",
                data: {id: id, _token: token},
                success: function (data) {
                    $('.tasnif').html('<option selected disabled> اختر التصنيف</option>');
                    console.log(data.user);
                    data.user.forEach(function (val){
                        $(".tasnif").append('<option value="' + val.id + '">' + val.arabic_name + '</option>');

                    });

                    if (data.status !== "ok") {
                        alert("ERROR");
                    }

                },
                error: function () {
                    alert("ERROR");
                }
            })
        })

    </script>

@stop