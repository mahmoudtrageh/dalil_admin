<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    @include('site.assets.Site-CSS')
    <title>تسجيل حساب</title>

</head>

<body>
    <!--Contact form start-->
    <div class="contact-form ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section-title text-center">
                        <h2 class="mt-4" id="register">انشاء حساب</h2>
                    </div>
                </div>
            </div>
            @include('errors.errors')
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="contact-form">
                        <p class="form-messege"></p>
                        <form id="contact-form" action="{{route('site.register')}}#register" method="post">
                            {{csrf_field()}}

                            <input type="hidden" name="is_active" value="0">
                            <input class="form-control mb-2" type="text" name="name" value="{{old('name')}}" placeholder="الاسم">
                            <input class="form-control mb-2" type="email" name="email" value="{{old('email')}}" placeholder="البريد الالكتروني">
                            <input class="form-control mb-2" type="number" name="ph_number" value="{{old('ph_number')}}" placeholder="رقم الهاتف">
                            <input class="form-control mb-2" type="password" name="password" placeholder="الرقم السري">
                            <input class="form-control mb-2" type="password" name="password_confirmation" placeholder=" تاكيد الرقم السري">
                            <button class="btn btn-primary mb-2" type="submit">انشاء</button>
                        </form>
                    </div>
                    <span class="login"><a href="{{route('site.get.login')}}"><p>تسجيل دخول</p></a></span>
                </div>
            </div>
        </div>
    </div>
    <!--Contact form end-->

    @include('site.assets.Site-JS')
    <script>

        setTimeout(fade_out, 5000);

        function fade_out() {
            $("#checker").fadeOut().empty();
        }

    </script>
</body>
</html>