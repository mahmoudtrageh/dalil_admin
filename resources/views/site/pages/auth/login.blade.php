<!DOCTYPE html>


<html lang="en"  direction="rtl" style="direction: rtl;" >
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    @include('site.assets.Site-CSS')
</head>

<body>

@include('errors.errors')

    <!--Contact form start-->
    <div class="contact-form ptb-100 mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section-title text-center">
                        <h2 id="login" class="mb-3">تسجيل الدخول</h2>
                        <img src="{{asset('site/img/05.jpg')}}" width="100px" heigt="100px">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="contact-form">
                        <p class="form-messege"></p>
                        <form id="contact-form" action="{{route('site-login-check')}}" method="post">
                            {{csrf_field()}}
                            <input class="form-control mb-2" type="text" name="email" placeholder="البريد الالكتروني">
                            <input class="form-control mb-2" type="password" name="password" placeholder="الرقم السري">
                            <div class="form-group">تذكرني <input type="checkbox" name="remember_me">
                            </div>
                            <button class="btn btn-primary mb-2" type="submit">دخول</button>
                        </form>
                        <span class="register"><a href="{{route('site.get.register')}}"><p>تسجيل حساب</p></a></span>
                        <span><a href="{{route('site-forget')}}"><p>نسيت كلمة السر</p></a></span>

                </div>
            </div>
        </div>
    </div>
    </div>
    <!--Contact form end-->
    @include('site.assets.Site-JS')

</body>

</html>
