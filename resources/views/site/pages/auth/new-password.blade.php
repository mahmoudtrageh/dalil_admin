@extends('site.layouts.Master-Layout')

@section('title')



@stop

@section('content')

    <!--Breadcrumbs start-->
    <div class="breadcrumbs text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumbs-title">
                        <h2>تغير كلمه المرور</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumbs end-->
    <!--Contact form start-->
    <div class="contact-form ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section-title text-center">
                        <h2 id="change-password">تغير كلمه المرور</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="contact-form">
                        <p class="form-messege"></p>
                        <form id="contact-form" action="{{route('site.pass.change')}}#change-password" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="email" value="{{$email}}"/>
                            <input  type="password" name="password" placeholder=" كلمه المرور الجديدة">
                            <input  type="password" name="password_confirmation" placeholder=" تاكيد كلمه المرور الجديدة">
                            <button type="submit">تاكيد التغير</button>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--Contact form end-->

@stop