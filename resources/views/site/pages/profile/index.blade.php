@extends('site.layouts.Master-Layout')

@section('title')

    حسابك

@stop

@section('content')
    <!--Contact form start-->
    <div class="contact-form ptb-100" style="height: 770px;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section-title text-center">
                        <h2 id="profile">حسابك</h2>
                    </div>
                </div>
            </div>
            @include('errors.errors')
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="contact-form">
                        <p class="form-messege"></p>
                        <form id="contact-form" action="{{route('site.update.profile')}}#profile" method="post">
                            {{csrf_field()}}
                            <input class="form-control mb-2" type="text" name="name" value="{{auth()->user()->name}}" placeholder="الاسم">
                            <input class="form-control mb-2" type="email" name="email" value="{{auth()->user()->email}}" placeholder="البريد الالكتروني">
                            <input class="form-control mb-2" type="number" name="ph_number" value="{{auth()->user()->ph_number}}" placeholder="الرقم الموبايل">
                            <button class="btn btn-primary mb-4" type="submit">تاكيد التغير</button>
                        </form>
                        <button href="#" data-toggle="modal" data-target="#myModal" class="form-control mb-5 btn btn-primary">
                         تغير الرقم السري</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!--Contact form end-->


    <!-- Modal -->
    <div class="modal fade  model-togel" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <br> <br>
                <div class="inp container">
                    <form action="{{route('site.update.password')}}#profile" method="post">
                        {{csrf_field()}}
                        <input class="form-control mb-2" type="text" name="old_password" placeholder="كلمه المرور القديمه">
                        <input class="form-control mb-2" type="password" name="new_password" placeholder="كلمه المرور الجديده">
                        <input class="form-control mb-2" type="password" name="new_password_confirmation" placeholder=" تأكيد كلمه المرور الجديده">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save Change</button>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


@stop