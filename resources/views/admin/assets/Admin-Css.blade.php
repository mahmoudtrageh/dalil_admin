<link rel="apple-touch-icon" href="{{asset('app-assets/images/ico/apple-icon-120.png')}}">
<link rel="shortcut icon" type="image/x-icon" href="{{asset('app-assets/images/ico/favicon.ico')}}">
<link rel="shortcut icon" type="image/x-icon" href="{{asset('app-assets/images/ico/favicon-32.png')}}">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
      rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
      rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/admin.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css-rtl/vendors.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('app-assets/css-rtl/app.css')}}">
