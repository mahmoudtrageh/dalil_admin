<!DOCTYPE html>


<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    @include('admin.assets.Admin-Css')
</head>

<body>

@include('errors.errors')

<!--Contact form start-->
    <div class="container">
        <div class="col-md-6" style="margin:0 auto;">
            <div class="row">
                    <div class="section-title mt-3" style="margin:0 auto;">
                        <h2 id="login" class="mb-3">تسجيل الدخول</h2>
                        <img class="mb-2" src="{{asset('site/img/05.jpg')}}" width="100px" heigt="100px">
                    </div>
            </div>
        </div>

        <div class="col-md-6" style="margin:0 auto;">
                <div class="contact-form">
                            <form action="{{route('admin.login')}}" method="post">
                                {{csrf_field()}}
                                <input class="form-control mb-2" type="text" placeholder="Name" name="name" autocomplete="off" >
                                <input class="form-control mb-2" type="password" placeholder="Password" name="password" >
                                <button type="submit" class="btn btn-primary">دخول</button>
                            </form>
                </div>
        </div>
    </div>
<!--Contact form end-->
@include('admin.assets.Admin-JS')

</body>

</html>