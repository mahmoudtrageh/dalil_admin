@extends('admin.layouts.Master-Layout')

@section('title')
    تعديل الرقم السري
@stop

@section('content')


    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">الملف الشخصى</h3>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-xl-12 col-lg-4">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab"
                                       href="#m_user_profile_tab_1" role="tab">
                                        <i class="flaticon-share m--hide"></i>
                                        تغير كلمة المرور
                                    </a>
                                </li>

                            </ul>
                        </div>

                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_user_profile_tab_1">
                            <form action="{{route('update.profile.password.admin')}}" method="post" class="m-form m-form--fit m-form--label-align-right">
                                {{csrf_field()}}
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group m--margin-top-10 m--hide">
                                        <div class="alert m-alert m-alert--default" role="alert">
                                            The example form below demonstrates common HTML form elements that
                                            receive updated styles from Bootstrap with additional classes.
                                        </div>
                                    </div>


                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label"
                                               style="text-align:right;">كلمة المرور القديمة</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" name="old_password" type="password" value="">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label"
                                               style="text-align:right;">كلمة المرور الجديدة</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="password" name="new_password" value="">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-3 col-form-label"
                                               style="text-align:right;"> تاكيد كلمة المرور الجديدة</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="password" name="new_password_confirmation" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-7">
                                                <button type="submit"
                                                        class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                    خفظ
                                                </button>&nbsp;&nbsp;

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane " id="m_user_profile_tab_2">

                        </div>
                        <div class="tab-pane " id="m_user_profile_tab_3">

                        </div>
                        @include('errors.errors')
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop