@extends('admin.layouts.Master-Layout')

@section('title')
    تعديل البروفايل
@stop

@section('content')

    <div class="container">
        <form action="{{route('update.profile.admin')}}" method="post" enctype="multipart/form-data" class="m-form m-form--fit m-form--label-align-right">
        {{csrf_field()}}

        <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title mt-5">تعديل الصورة</h3>
                    </div>
                </div>
            </div>
            <!-- END: Subheader -->
            <div class="m-content">
                <div class="row">
                    <div class="col-xl-3 col-lg-4">
                        <div class="m-portlet m-portlet--full-height  ">
                            <div class="m-portlet__body">
                                <div class="m-card-profile">


                                        <img src="{{auth()->guard('admin')->user()->photo ?  asset(auth()->guard('admin')->user()->photo->file) : 'https://place-hold.it/300'}}"
                                             class="img-fluid mb-5" alt=""/>
                                    <div class="m-card-profile__pic">
                                        <div class="wrap-custom-file inline-block mb-10">
                                            <input type="file"
                                                   name="photo_id"
                                                   id="img2"
                                                   accept=".gif, .jpg, .png"/>
                                            <label for="img2"
                                                   class="file-ok"

                                                   style="background-image: url({{auth()->guard('admin')->user()->photo ?  asset(auth()->guard('admin')->user()->photo->file) : 'https://place-hold.it/300'}});"
                                            >
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="m-portlet__body-separator"></div>

                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-8">
                        <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                            <div class="m-portlet__head">
                                <div class="m-portlet__head-tools">
                                    <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                                        role="tablist">
                                        <li class="nav-item m-tabs__item">
                                            <a class="nav-link m-tabs__link active" data-toggle="tab"
                                               href="#m_user_profile_tab_1" role="tab">
                                                <i class="flaticon-share m--hide"></i>
                                                تعديل البيانات
                                            </a>
                                        </li>

                                    </ul>
                                </div>

                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="m_user_profile_tab_1">

                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group m--margin-top-10 m--hide">
                                            <div class="alert m-alert m-alert--default" role="alert">
                                                The example form below demonstrates common HTML form elements that
                                                receive updated styles from Bootstrap with additional classes.
                                            </div>
                                        </div>


                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label"
                                                   style="text-align:center;">الاسم</label>
                                            <div class="col-7">
                                                <input class="form-control m-input" name="name"  type="text" value="{{$admin->name}}">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label for="example-text-input" class="col-2 col-form-label"
                                                   style="text-align:center;">البريد الإلكترونى</label>
                                            <div class="col-7">
                                                <input class="form-control m-input" type="text" name="email" value="{{$admin->email}}">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="m-portlet__foot m-portlet__foot--fit">
                                        <div class="m-form__actions">
                                            <div class="row">
                                                <div class="col-2">
                                                </div>
                                                <div class="col-7">
                                                    <button type="submit"
                                                            class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                        خفظ
                                                    </button>&nbsp;&nbsp;

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane " id="m_user_profile_tab_2">

                                </div>
                                <div class="tab-pane " id="m_user_profile_tab_3">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('errors.errors')
            </div>
        </form>

    </div>

    @stop
