@extends('admin.layouts.Master-Layout')

@section('title')
    الشركات
@stop

@section('content')

    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">الشركات</h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item active">الشركات
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content-body">
            <!-- Basic Initialisation table -->
            <section id="initialisation " class="box-shadow">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="content-header-title mt-2"><i class="fas fa-user-md m-1"></i>الشركات</h3>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-outline-primary" data-toggle="modal"
                                                    data-target="#add">
                                                اضف
                                            </button>
                                            <button type="button" class="btn btn-outline-primary pull-left mx-1">تصدير
                                                اكسيل
                                            </button>
                                            <button type="button" class="btn btn-outline-primary pull-left">طباعة
                                            </button>
                                            <!-- filters-->
                                            <form method="get" action="{{route('company.filter')}}">
                                                <section class="mt-1">
                                                    <div class="row">
                                                        <div class="col-xl-3">
                                                            <div class="card">
                                                                <div class="card-body p-0">
                                                                    <div class="form-group">
                                                                        <select name="country"
                                                                                class="country form-control">

                                                                            <option disabled selected>اختر
                                                                                الدولة
                                                                            </option>

                                                                            @foreach($countries as $country)

                                                                                <option
                                                                                        value="{{$country->id}}">{{$country->arabic_name}}
                                                                                </option>

                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <select name="section"
                                                                                class="section form-control">
                                                                            <option selected disabled>  اختر القطاع</option>
                                                                            @foreach($sections as $section)

                                                                                <option
                                                                                        value="{{$section->id}}">{{$section->arabic_name}}
                                                                                </option>

                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3">
                                                            <div class="card">
                                                                <div class="card-body p-0">
                                                                    <div class="form-group">
                                                                        <select name="mohafza"
                                                                                class="mohafza form-control">

                                                                            <option selected disabled>  اختر الدولة أولًا</option>

                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <select name="tasnif"
                                                                                class="tasnif form-control">
                                                                            <option selected disabled>  اختر القطاع أولًا</option>

                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <button type="submit"
                                                                                class="btn btn-primary btn-min-width mr-1 mb-1">
                                                                            <i class="la la-search"></i> بحث
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3">
                                                            <div class="card-body p-0">
                                                                <div class="form-group ">
                                                                    <select name="city" class="city form-control">
                                                                        <option selected disabled>  اختر المحافظة أولًا</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" name="code" class="form-control"
                                                                           placeholder="الكود ">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3">
                                                            <div class="card-body p-0">
                                                                <div class="form-group ">
                                                                    <select name="zone" class="zone form-control">
                                                                        <option selected disabled>  اختر المدينة أولًا</option>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group">
                                                                    <input type="text" name="name" class="form-control"
                                                                           placeholder="اسم الشركة ">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </form>
                                            <!-- #filters-->
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered order-column dataex-basic-initialisation table-responsive" style="overflow-x: scroll;">
                                        <thead>
                                        <tr>
                                            <th>اسم الشركة</th>
                                            <th>الكود</th>
                                            <th>الدولة</th>
                                            <th>المحافظة</th>
                                            <th>المدينة</th>
                                            <th>المنطقة</th>
                                            <th>القطاع</th>
                                            <th>التصنيف</th>
                                            <th>العنوان</th>
                                            <th>رقم التيليفون</th>
                                            <th>اسم المسؤول</th>
                                            <th>رقم الموبايل</th>
                                            <th>رقم الواتس</th>
                                            <th> الإيميل</th>
                                            <th>الإجراء</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>

                                            @if($companies)
                                                @foreach($companies as $company)

                                                    <td>{{$company->name}}</td>
                                                    <td>{{$company->code}}</td>
                                                    <td> {{$company->country->arabic_name}}</td>
                                                    <td> {{$company->mohafza->arabic_name}}</td>
                                                    <td>{{$company->city->arabic_name}}</td>
                                                    <td>{{$company->zone->arabic_name}}</td>
                                                    <td>{{$company->section->arabic_name}}</td>
                                                    <td>{{$company->tasnif->arabic_name}}</td>
                                                    <td>{{$company->address}}</td>
                                                    <td>{{$company->phone_number}}</td>
                                                    <td>{{$company->res_name}}</td>
                                                    <td>{{$company->mobile_number}}</td>
                                                    <td>{{$company->whatsapp_number}}</td>
                                                    <td>{{$company->email}}</td>
                                                    <td>
                                                        <button type="button"
                                                                data-target="#delete-company{{$company->id}}"
                                                                data-toggle="modal"
                                                                class="btn btn-outline-danger btn-sm"><i
                                                                    class="fas fa-trash-alt"></i></button>
                                                        <button type="button"
                                                                data-target="#edit-company{{$company->id}}"
                                                                class="btn btn-outline-info mt-1 btn-sm"
                                                                data-toggle="modal"><i class="fas fa-edit"></i></button>
                                                    </td>


                                        </tr>

                                        @endforeach
                                        @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Basic Initialisation table -->
        </div>
    </div>

    <!-- add Modal -->
    <div class="modal animated zoomInLeft text-left" id="add" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-right">اضافة شركة</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="border-1px p-25">
                                <form action="{{route('add.company')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input name="name" class="form-control" type="text"
                                                       placeholder="اسم الشركة">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input name="code" class="form-control" type="text"
                                                       placeholder="كود الشركة ">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select class="country form-control" name="country_id">
                                                    <option selected disabled>الدولة</option>

                                                    @foreach($countries as $country)

                                                        <option
                                                                value="{{$country->id}}">{{$country->arabic_name}}
                                                        </option>

                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select class="mohafza form-control" name="mohafza_id">

                                                    <option disabled selected>اختر الدولة أولًا</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select class="city form-control" name="city_id">
                                                    <option selected disabled>اختر المحافظة أولًا</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select class="zone form-control" name="zone_id">
                                                    <option selected disabled>اختر المدينة أولًا</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select class="section form-control" name="section_id">
                                                    <option disabled selected>القطاع</option>
                                                    @foreach($sections as $section)

                                                        <option
                                                                value="{{$section->id}}">{{$section->arabic_name}}
                                                        </option>

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <select class="tasnif form-control" name="tasnif_id">
                                                    <option disabled selected>اختر القطاع أولًا</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input name="address" class="form-control" type="text"
                                                       placeholder="العنوان ">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input name="phone_number" class="form-control" type="number"
                                                       placeholder="رقم التيليفون ">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input name="res_name" class="form-control" type="text"
                                                       placeholder="اسم المسؤول ">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input name="mobile_number" class="form-control" type="number"
                                                       placeholder="رقم الموبايل  ">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input name="whatsapp_number" class="form-control" type="number"
                                                       placeholder="رقم الواتس  ">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input name="email" class="form-control" type="email"
                                                       placeholder="الإيميل">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-outline-info" value="أضف">
                                        <button type="button" class="btn grey btn-outline-danger" data-dismiss="modal">
                                            غلق
                                        </button>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@if($companies)
    @foreach($companies as $company)

        <!-- Modal  edit-->
        <div class="modal animated zoomInLeft text-left" id="edit-company{{$company->id}}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title text-right">تعديل </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="border-1px p-25">

                                    <!-- Contact Form -->
                                    <form action="{{route('edit.company')}}" method="post">
                                        {{csrf_field()}}

                                        <input type="hidden" name="id" value="{{$company->id}}"/>


                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input name="name" class="form-control" type="text"
                                                           value="{{$company->name}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input name="code" class="form-control" type="text"
                                                           value="{{$company->code}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <select class="country form-control" name="country">
                                                        <option>{{$company->country->arabic_name}}</option>

                                                        @foreach($countries as $country)

                                                            <option
                                                                    value="{{$country->id}}">{{$country->arabic_name}}
                                                            </option>

                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <select class="mohafza form-control" name="mohafza">
                                                        <option>{{$company->mohafza->arabic_name}} </option>
                                                        @foreach($mohafzas as $mohafza)

                                                            <option
                                                                    value="{{$mohafza->id}}">{{$mohafza->arabic_name}}
                                                            </option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <select class="city form-control" name="city">
                                                        <option>{{$company->city->arabic_name}} </option>
                                                        @foreach($cities as $city)

                                                            <option
                                                                    value="{{$city->id}}">{{$city->arabic_name}}
                                                            </option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <select class="zone form-control" name="zone">
                                                        <option>{{$company->zone->arabic_name}} </option>
                                                        @foreach($zones as $zone)

                                                            <option
                                                                    value="{{$zone->id}}">{{$zone->arabic_name}}
                                                            </option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <select class="section form-control" name="section">
                                                        <option>{{$company->section->arabic_name}} </option>
                                                        @foreach($sections as $section)

                                                            <option
                                                                    value="{{$section->id}}">{{$section->arabic_name}}
                                                            </option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <select class="tasnif form-control" name="tasnif">
                                                        <option>{{$company->tasnif->arabic_name}} </option>
                                                        @foreach($tasnifs as $tasnif)

                                                            <option
                                                                    value="{{$tasnif->id}}">{{$tasnif->arabic_name}}
                                                            </option>

                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input name="address" class="form-control" type="text"
                                                           value="{{$company->address}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input name="phone_number" class="form-control" type="number"
                                                           value="{{$company->phone_number}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input name="res_name" class="form-control" type="text"
                                                           value="{{$company->res_name}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input name="mobile_number" class="form-control" type="number"
                                                           value="{{$company->mobile_number}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input name="whatsapp_number" class="form-control" type="number"
                                                           value="{{$company->whatsapp_number}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input name="email" class="form-control" type="email"
                                                           value="{{$company->email}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-outline-info" value="تعديل">
                                            <button type="button" class="btn grey btn-outline-danger"
                                                    data-dismiss="modal">غلق
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    @endforeach
@endif

@if($companies)
    @foreach($companies as $company)

        <!-- Modal delete-->
        <div class="modal animated zoomInLeft text-left" id="delete-company{{$company->id}}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title text-right">حذف </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                    </div>

                    <form action="{{route('delete.company')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$company->id}}"/>
                        <div class="modal-body">
                            <p style="font-size:148%;">هل تريد اكمال العملية</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">اغلاق</button>
                            <button type="submit" class="btn btn-success">تاكيد</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    @endforeach
    @endif

@stop

@section('scripts')


        <script>
            $(document).on("change", ".country", function (e) {
                e.preventDefault();
                var id = $(this).val();
                var token = "{{ csrf_token() }}";

                $.ajax({
                    url: "{{ route('admin.get.mohafza') }}",
                    type: "post",
                    dataType: "json",
                    data: {id: id, _token: token},
                    success: function (data) {
                        $('.mohafza').html('<option selected disabled> اختر المحافظة</option>');
                        data.user.forEach(function (val){
                            $(".mohafza").append('<option value="' + val.id + '">' + val.arabic_name + '</option>');
                        });

                        if (data.status !== "ok") {
                            alert("ERROR");
                        }

                    },
                    error: function () {
                        alert("ERROR");
                    }
                })
            })


            $(document).on("change", ".mohafza", function (e) {
                e.preventDefault();
                var id = $(this).val();
                var token = "{{ csrf_token() }}";

                $.ajax({
                    url: "{{ route('admin.get.city') }}",
                    type: "post",
                    dataType: "json",
                    data: {id: id, _token: token},
                    success: function (data) {
                        $('.city').html('<option selected disabled> اختر المدينة</option>');
                        console.log(data.user);
                        data.user.forEach(function (val){
                            $(".city").append('<option value="' + val.id + '">' + val.arabic_name + '</option>');

                        });

                        if (data.status !== "ok") {
                            alert("ERROR");
                        }

                    },
                    error: function () {
                        alert("ERROR");
                    }
                })
            })

            $(document).on("change", ".city", function (e) {
                e.preventDefault();
                var id = $(this).val();
                var token = "{{ csrf_token() }}";

                $.ajax({
                    url: "{{ route('admin.get.zone') }}",
                    type: "post",
                    dataType: "json",
                    data: {id: id, _token: token},
                    success: function (data) {
                        $('.zone').html('<option selected disabled> اختر المنطقة</option>');
                        console.log(data.user);
                        data.user.forEach(function (val){
                            $(".zone").append('<option value="' + val.id + '">' + val.arabic_name + '</option>');

                        });

                        if (data.status !== "ok") {
                            alert("ERROR");
                        }

                    },
                    error: function () {
                        alert("ERROR");
                    }
                })
            })

            $(document).on("change", ".section", function (e) {
                e.preventDefault();
                var id = $(this).val();
                var token = "{{ csrf_token() }}";

                $.ajax({
                    url: "{{ route('admin.get.tasnif') }}",
                    type: "post",
                    dataType: "json",
                    data: {id: id, _token: token},
                    success: function (data) {
                        $('.tasnif').html('<option selected disabled> اختر التصنيف</option>');
                        console.log(data.user);
                        data.user.forEach(function (val){
                            $(".tasnif").append('<option value="' + val.id + '">' + val.arabic_name + '</option>');

                        });

                        if (data.status !== "ok") {
                            alert("ERROR");
                        }

                    },
                    error: function () {
                        alert("ERROR");
                    }
                })
            })

        </script>

    @stop