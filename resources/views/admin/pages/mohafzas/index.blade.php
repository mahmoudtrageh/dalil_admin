@extends('admin.layouts.Master-Layout')

@section('title')
    المحافظات
@stop

@section('content')

    <!--الاطباء-->
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">المحافظات
                </h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item active">  المحافظات

                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        @include('errors.errors')

        <div class="content-body">
            <!-- Basic Initialisation table -->
            <section id="initialisation " class="box-shadow">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="content-header-title mt-2"><i class="fas fa-user-lock ml-1"></i>المحافظات</h3>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="col-md-12 col-sml-12">
                                        <div class="form-group">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#zoomInLeft">
                                                أضف
                                            </button>
                                            <button type="button" class="btn btn-outline-primary pull-left mx-1" >تصدير اكسيل</button>
                                            <button type="button" class="btn btn-outline-primary pull-left" >طباعة</button>
                                        </div>
                                        <table class="table table-bordered table-striped ">
                                            <thead>


                                            <tr >
                                                <th>الاسم العربي  </th>
                                                <th>الاسم الانجليزي  </th>
                                                <th>المدن  </th>
                                                <th>الاجراء</th>
                                            </tr>
                                            </thead>
                                            <tbody>


                                            <tr >

                                                @if($mohafzas)

                                                    @foreach($mohafzas as $mohafza)


                                                        <td > {{$mohafza->arabic_name}}</td>
                                                        <td >{{$mohafza->english_name}}</td>
                                                        <td >
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <button type="button" class="btn btn-outline-danger"><a href="{{route('cities.index',['id'=>$mohafza->id])}}">اضغط هنا</a></button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <button type="button"  class="btn btn-outline-danger" data-toggle="modal"  data-target="#delete-mohafza{{$mohafza->id}}" ><i class="fas fa-trash-alt"></i></button>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <button type="button" data-toggle="modal" data-target="#edit-mohafza{{$mohafza->id}}" class="btn btn-outline-info mr-1"><i class="fas fa-edit"></i></button>
                                                                </div>
                                                            </div>
                                                        </td>
                                            </tr>

                                            @endforeach

                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Basic Initialisation table -->
        </div>
    </div>
    <!-- Modal add -->
    <div class="modal animated zoomInLeft text-left" id="zoomInLeft" tabindex="-1" role="dialog" aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-right">أضف</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="border-1px p-25">
                                <!-- Contact Form -->
                                <form action="{{route('add.mohafza')}}" method="post">
                                    {{csrf_field()}}


                                    <input type="hidden" name="country_id" value="{{$id}}"/>


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="arabic_name" placeholder="الاسم العربي">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="english_name" placeholder="الاسم الانجليزي">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <input type="submit" class="btn btn-outline-info" value="أضف">


                                        <button type="button" class="btn grey btn-outline-danger" data-dismiss="modal">غلق</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@if($mohafzas)
    @foreach($mohafzas as $mohafza)

        <!-- Modal  edit-->
        <div class="modal animated zoomInLeft text-left" id="edit-mohafza{{$mohafza->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title text-right">تعديل   </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 ">
                                <div class="border-1px p-25">

                                    <!-- Contact Form -->
                                    <form action="{{route('edit.mohafza')}}" method="post">
                                        <div class="row">

                                            {{csrf_field()}}
                                            <input type="hidden" name="id" value="{{$id}}"/>


                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="arabic_name" placeholder="الاسم العربي" value="{{$mohafza->arabic_name}}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="english_name" placeholder="الاسم الانجليزي" value="{{$mohafza->english_name}}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <input type="submit" class="btn btn-outline-info">


                                            <button type="button" class="btn grey btn-outline-danger" data-dismiss="modal">غلق</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endforeach
@endif

@if($mohafzas)
    @foreach($mohafzas as $mohafza)

        <!-- Modal delete-->
        <div class="modal animated zoomInLeft text-left" id="delete-mohafza{{$mohafza->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title text-right">حذف   </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>

                    <form action="{{route('delete.mohafza')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$id}}"/>
                        <div class="modal-body">
                            <p style="font-size:148%;">هل تريد اكمال العملية</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">اغلاق</button>
                            <button type="submit" class="btn btn-success">تاكيد</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    @endforeach
@endif

@stop