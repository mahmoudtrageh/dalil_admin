@extends('admin.layouts.Master-Layout')

@section('title')
    لوحة التحكم
@stop

@section('content')

    <!--مستفيدين-->
    <div class="content-wrapper ">
        <div class="content-header row">
            <h3 class="content-header-title">الإحصائيات </h3>
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-3">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$countries}}</h3>
                                        <h6>عدد الدول </h6>
                                    </div>
                                </div>
                                <div class="progress progress-bar-striped progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$cities}}</h3>
                                        <h6>عدد المدن </h6>
                                    </div>
                                </div>
                                <div class="progress progress-bar-striped progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$mohafzas}}</h3>
                                        <h6>عدد المحافظات </h6>
                                    </div>
                                </div>
                                <div class="progress progress-bar-striped progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$zones}}</h3>
                                        <h6>عدد المناطق </h6>
                                    </div>
                                </div>
                                <div class="progress progress-bar-striped progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-3">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$sections}}</h3>
                                        <h6>عدد القطاعات </h6>
                                    </div>
                                </div>
                                <div class="progress progress-bar-striped progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$tasnifs}}</h3>
                                        <h6>عدد التصنيفات </h6>
                                    </div>
                                </div>
                                <div class="progress progress-bar-striped progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$companies}}</h3>
                                        <h6>عدد الشركات </h6>
                                    </div>
                                </div>
                                <div class="progress progress-bar-striped progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card pull-up">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="media d-flex">
                                    <div class="media-body text-left">
                                        <h3 class="info">{{$users}}</h3>
                                        <h6>عدد المستخدمين </h6>
                                    </div>
                                </div>
                                <div class="progress progress-bar-striped progress-sm mt-1 mb-0 box-shadow-2">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- ////////////////////////////////////////////////////////////////////////////-->


@stop