@extends('admin.layouts.Master-Layout')

@section('title')
    المستخدمين
@stop

@section('content')

    <!--الاطباء-->
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-6 col-12 mb-2">
                <h3 class="content-header-title">المستخدمين
                </h3>
                <div class="row breadcrumbs-top">
                    <div class="breadcrumb-wrapper col-12">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">الرئيسية</a>
                            </li>
                            <li class="breadcrumb-item active">  المستخدمين

                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        @include('errors.errors')


        <div class="content-body">
            <!-- Basic Initialisation table -->
            <section id="initialisation " class="box-shadow">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="content-header-title mt-2"><i class="fas fa-user-lock ml-1"></i>المستخدمين</h3>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <div class="col-md-12 col-sml-12">
                                        <div class="form-group">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#zoomInLeft">
                                                أضف
                                            </button>
                                            <button type="button" class="btn btn-outline-primary pull-left mx-1" >تصدير اكسيل</button>
                                            <button type="button" class="btn btn-outline-primary pull-left" >طباعة</button>
                                        </div>
                                        <table class="table table-bordered table-striped ">
                                            <thead>
                                            <tr >
                                                <th>الاسم </th>
                                                <th>البريد الالكتروني </th>
                                                <th>رقم الهاتف  </th>
                                                <th scope="col">الحالة</th>
                                                <th>الاجراء</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if($users)

                                                @foreach($users as $user)

                                                    <tr >
                                                        <td >{{$user->name}}</td>
                                                        <td > {{$user->email}}</td>
                                                        <td > {{$user->ph_number}} </td>
                                                        <td style="border:1px solid #ccc;">
                                                            <select class="form-control stutus" uid="{{ $user->id }}">
                                                                <option @if($user->is_active) selected
                                                                        @endif value="1">
                                                                    نشط
                                                                </option>
                                                                <option @if(!$user->is_active) selected
                                                                        @endif value="0">
                                                                    معلق
                                                                </option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <button type="button" data-toggle="modal" data-target="#delete{{$user->id}}"  class="btn btn-outline-danger"><i class="fas fa-trash-alt"></i></button>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <button type="button" data-toggle="modal" data-target="#edit{{$user->id}}" class="btn btn-outline-info mr-1"><i class="fas fa-edit"></i></button>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                @endforeach

                                            @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        {{$users->links()}}
            <!--/ Basic Initialisation table -->



        </div>
    </div>
    <!-- Modal add -->
    <div class="modal animated zoomInLeft text-left" id="zoomInLeft" tabindex="-1" role="dialog" aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-right">أضف</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="border-1px p-25">

                                <!-- Contact Form -->
                                <form action="{{route('add.user')}}" method="post">
                                    {{csrf_field()}}
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="name" placeholder="الاسم">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" placeholder="البريد الإلكتروني">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="password" placeholder="كلمة المرور">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="number" class="form-control" name="ph_number" placeholder="رقم الهاتف">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="modal-footer">

                                        <input type="submit" class="btn btn-outline-info" value="أضف">

                                        <button type="button" class="btn grey btn-outline-danger" data-dismiss="modal">غلق</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@if($users)
    @foreach($users as $user)
<!-- Modal  edit-->
<div class="modal animated zoomInLeft text-left" id="edit{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-right">تعديل   </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="border-1px p-25">

                            <!-- Contact Form -->
                            <form action="{{route('edit.user')}}" method="post">
                                {{csrf_field()}}

                                <input type="hidden" name="id" value="{{$user->id}}"/>


                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="الاسم" value="{{$user->name}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" placeholder="البريد الإلكتروني" value="{{$user->email}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password" placeholder="الرقم السري" value="{{$user->password}}">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="number" class="form-control" name="ph_number" placeholder="رقم الهاتف" value="{{$user->ph_number}}">
                                        </div>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <input type="submit" value="تعديل" class="btn btn-outline-info">
                                    <button type="button" class="btn grey btn-outline-danger" data-dismiss="modal">غلق</button>
                                </div>
                            </form>
        </div>
    </div>
</div>
            </div>
        </div>
    </div>
            </div>
@endforeach
    @endif

@if($users)
    @foreach($users as $user)
<!-- Modal delete-->
<div class="modal animated zoomInLeft text-left" id="delete{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel71" style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-right">حذف   </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
                <div class="modal-body">
                    <form action="{{route('delete.user')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$user->id}}"/>
                        <p style="font-size:148%;">هل تريد اكمال العملية</p>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">اغلاق</button>
                            <button type="submit" class="btn btn-success">تاكيد</button>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>

@endforeach
    @endif
@stop


@section('scripts')

    <script>
        $(document).on("change", ".stutus", function () {
            var status = $(this).val();
            var id = $(this).attr("uid");
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('update.status.user') }}",
                type: "post",
                dataType: "json",
                data: {status: status, id: id, _token: token},
                success: function (data) {
                    console.log(data.status);
                    if (data.status !== "ok") {
                        alert("ERROR");
                    }

                },
                error: function () {
                    alert("ERROR");
                }
            })
        })
    </script>


@stop