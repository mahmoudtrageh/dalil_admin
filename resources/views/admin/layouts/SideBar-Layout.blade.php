<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" >
            <li class=" nav-item"><a href="{{url('admin/index')}}"><i class="la la-home"></i><span class="menu-title" data-i18n="nav.dash.main"> لوحة التحكم </span></a>
            </li>
            @if(Has_permissions(3))
            <li class=" nav-item"><a href="{{url('admin/supervisors/')}}"><i class="fas fa-unlock-alt"></i><span class="menu-title" data-i18n="nav.dash.main">المشرفين</span></a>
            </li>
            @endif
            @if(Has_permissions(2))
            <li class=" nav-item"><a href="{{url('admin/users/')}}"><i class="fas fa-people-carry"></i><span class="menu-title" data-i18n="nav.dash.main">المستخدمين</span></a>
            </li>
            @endif
            @if(Has_permissions(4))
            <li class=" nav-item"><a href="{{url('admin/countries/')}}"><i class="fab fa-accessible-icon"></i><span class="menu-title" data-i18n="nav.dash.main">الدول</span></a>
            </li>
            @endif
            @if(Has_permissions(5))
            <li class=" nav-item"><a href="{{url('admin/sections/')}}"><i class="fas fa-user-md"></i><span class="menu-title" data-i18n="nav.dash.main">القطاعات</span></a>
            </li>
            @endif
            @if(Has_permissions(6))
            <li class=" nav-item"><a href="{{url('admin/companies/')}}"><i class="fab fa-medrt"></i><span class="menu-title" data-i18n="nav.dash.main">الشركات </span></a>
            </li>
            @endif
            <li class=" nav-item"><a href="contact.html"><i class="fas fa-user-tie"></i><span class="menu-title" data-i18n="nav.support_raise_support.main">تواصل معنا</span></a>
            </li>
        </ul>
    </div>
</div>