<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="rtl">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="yehia ali yehia">  <title>دليل الشركات</title>


    <title>@yield('title')</title>
    @include('admin.assets.Admin-Css')
    @yield('css')



</head>
{{-- end head --}}

<body class="vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">

<!-- fixed-top-->
<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light bg-cyan bg-darken-3 navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                <li class="nav-item">
                    <a class="navbar-brand" href="index.html">
                        <img class="brand-logo" alt="modern admin logo" src="{{asset('app-assets/images/logo/logo.png')}}">
                        <h3 class="brand-text"> Dalil Admin</h3>
                    </a>
                </li>
                <li class="nav-item d-md-none">
                    <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                    <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-user nav-item">

                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                <span class="mr-1">Hello,
                  <span class="user-name text-bold-700">{{ auth()->guard('admin')->user()->name }}</span>
                </span>
                            <span class="avatar avatar-online">


                  <img src="{{auth()->guard('admin')->user()->photo ?  asset(auth()->guard('admin')->user()->photo->file) : 'https://place-hold.it/300'}}"
                       class="m--img-rounded m--marginless" alt=""/>
<i></i></span>

                        </a>
                        <div class="dropdown-menu dropdown-menu-right">

                            <a class="dropdown-item" href="{{route('profile.index',['id'=>auth()->guard('admin')->user()->id])}}"><i class="ft-user"></i> Edit Profile</a>

                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" href="{{route('profile.change.password')}}">Password Setting</a>

                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" href="{{route('admin.logout')}}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{route('admin.logout')}}" method="get" style="display: none;">
                                {{csrf_field()}}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- //////////////////////////////////////////////////////////////////////////// End Nav-->

@include('admin.layouts.SideBar-Layout')

@include('errors.errors')

<div class="app-content content mt-3">
    @yield('content')
</div>


<footer class="footer footer-static footer-light navbar-border navbar-shadow">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 <a class="text-bold-800 grey darken-2"
                                                                                     target="_blank">Karim Helmy </a> </span>
        <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block"></span>
    </p>
</footer>

@include('admin.assets.Admin-JS');


@yield('scripts')

</body>
</html>