<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //

    protected  $fillable = [

        'name', 'code','country_id', 'mohafza_id', 'city_id', 'zone_id', 'section_id', 'tasnif_id', 'phone_number',
        'res_name', 'mobile_number', 'whatsapp_number', 'email', 'address',

    ];

    protected $table = 'companies';


    public function country(){
        return $this->belongsTo('App\Country');
    }

    public function mohafza(){
        return $this->belongsTo('App\Mohafza');
    }

    public function city(){
        return $this->belongsTo('App\City');
    }

    public function zone(){
        return $this->belongsTo('App\Zone');
    }

    public function section (){
        return $this->belongsTo('App\Section');
    }

    public function tasnif(){
        return $this->belongsTo('App\Tasnif');
    }
}

