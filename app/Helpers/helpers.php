<?php

if (!function_exists('Has_permission')) {
    function Has_permissions($role_id)
    {
        if (in_array($role_id, auth()->guard('admin')->user()->roles->pluck('id')->toArray())
            || auth()->guard('admin')->user()->id == 2) {
            return true;
        }
        return false;
    }
}
