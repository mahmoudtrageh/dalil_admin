<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mohafza extends Model
{
    //

    protected  $fillable = [

        'arabic_name', 'english_name', 'id','country_id',

        ];

    protected $table = 'mohafzat';

    public function cities(){
        return $this->hasMany('App\City');
    }

    public function companies(){
        return $this->hasMany('App\Company');
    }
}
