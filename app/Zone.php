<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    //

    protected $fillable = [
        'arabic_name', 'english_name', 'id','city_id'
    ];

    protected $table = 'zones';

    public function companies(){
        return $this->hasMany('App\Company');
    }
}
