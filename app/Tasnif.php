<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasnif extends Model
{
    //

    protected $fillable = [
        'arabic_name', 'english_name', 'id','section_id',
    ];

    protected $table = 'tasnifs';

    public function companies(){
        return $this->hasMany('App\Company');
    }
}
