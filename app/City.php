<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //

    protected $fillable = [
        'arabic_name', 'english_name', 'id','mohafza_id'
    ];

    protected $table = 'cities';

    public function zones(){
        return $this->hasMany('App\Zone');
    }
    public function companies(){
        return $this->hasMany('App\Company');
    }
}
