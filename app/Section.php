<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    //

    protected $fillable = [
        'arabic_name', 'english_name', 'id',
    ];

    protected $table = 'sections';


    // many to

    public function tasnifs(){
        return $this->hasMany('App\Tasnif');
    }

    public function companies(){
        return $this->hasMany('App\Company');
    }
}
