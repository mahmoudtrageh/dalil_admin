<?php

namespace App\Http\Controllers\Admin;


use App\City;
use App\Http\Controllers\Controller;

use App\Zone;
use Illuminate\Http\Request;

class ZonesController extends Controller
{
    //

    public function index($id)
    {

        $zones = City::find($id)->zones;
        return view('admin.pages.zones.index', compact('zones', 'id'));

    }

    public function add_zone(Request $request)
    {
        $this->validate($request, [
            'arabic_name' => 'required',
            'english_name' => 'required',
        ],
            [
                'arabic_name.required' => 'برجاء إدخال الاسم العربي',
                'english_name.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);

        if (Zone::create($request->all())) {
            return redirect()->back()->withSuccess('تم الاضافة');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function edit_zone(Request $request)
    {
        $this->validate($request, [
            'arabic_name' => 'required',
            'english_name' => 'required',
        ],
            [
                'arabic_name.required' => 'برجاء إدخال الاسم العربي',
                'english_name.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);
        $checker = Zone::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess('تم التعديل');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function delete_zone(Request $request)
    {
        Zone::find($request->id)->delete();
        return redirect()->back()->with('deleted', 'تم الحذف');

    }
}