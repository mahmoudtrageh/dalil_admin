<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationsConrtoller extends Controller
{
    // New User
    public function get_notifications()
    {
        return auth()->guard('admin')->user()->unreadNotifications->where('type', 'App\Notifications\NewUser');
    }

    public function read(Request $request)
    {
        \Auth::guard('admin')->user()->unreadNotifications()->find($request->id)->MarkAsRead();
        return ['url' => route('users.index')];
    }

//// New Inquiry
//    public function get_inquiry_notifications()
//    {
//        return auth()->guard('admin')->user()->unreadnotifications->where('type', 'App\Notifications\InquiryNotification');
//    }
//
//    public function read_inquiry(Request $request)
//    {
//        \Auth::guard('admin')->user()->unreadNotifications()->find($request->id)->MarkAsRead();
//        return ['url' => route('settings.index')];
//    }
}
