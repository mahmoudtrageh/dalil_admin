<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;


use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class ProfileController extends Controller
{
    //

    public function index()
    {
//        $admin = User::find($id);
        $admin =  User::find(auth()->guard('admin')->user()->id);
        return view('admin.pages.profile.index', compact('admin'));
    }

    public function update_profile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:visors,name,' . auth()->guard('admin')->user()->id,
            'email' => 'required|unique:visors,name,' . auth()->guard('admin')->user()->id,
            'photo_id' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ],
            [
                'name.required' => 'برجاء إدخال إسم المستخدم',
                'name.unique' => ' إسم المستخدم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الالكتروني',
                'email.unique' => ' البريد الالكتروني مسجل بالفعل',
            ]);
        $checker = User::find(auth()->guard('admin')->user()->id);
        $input = $request->all();

        if($file = $request->file('photo_id')){

            $name = time() . $file->getClientOriginalName();

            $file->move('images', $name);

            $photo = Photo::create(['file'=>$name]);

            $input['photo_id'] = $photo->id;

        }

        $checker->update($input);
        return redirect()->back()->withSuccess('تم تعديل البيانات بنجاح');
    }


    public function update_profile_password(Request $request)
    {
        $this->validate($request, [

            'old_password' => 'required',
            'new_password' => 'required|min:6|max:15',
            'new_password_confirmation' => 'required|same:new_password',

        ],

            [
                'old_password.required' => 'برجاء إدخال كلمه المرور القديمه',
                'new_password.required' => 'برجاء إدخال كلمه المرور الجديده',
                'new_password.min' => ' كلمه المرور الجديده لا تقل عن 6 حروف',
                'new_password.max' => ' كلمه المرور الجديده لا تزيد عن 15 حروف',
                'new_password_confirmation.required' => 'برجاء إدخال تأكيد كلمه المرور الجديده',
                'new_password_confirmation.same' => 'تأكيد كلمه المرور الجديده لا تتطابق',

            ]);

        $admin = User::find(auth()->guard('admin')->user()->id);
        $input = $request->all();
        $input['new_password'] = bcrypt($request->new_password);
        if (Hash::check($request->old_password, $admin->password)) {
            $admin->update(['password' => $input['new_password']]);
            session()->flash('success', 'تم تحديث كلمه المرور بنجاح');
            return redirect()->back();
        }
        session()->flash('error', 'كلمه المرور القديمه غير صحيحه');
        return redirect()->back();
    }

    public function change_password()
    {

        return view('admin.pages.profile.password');

    }

}
