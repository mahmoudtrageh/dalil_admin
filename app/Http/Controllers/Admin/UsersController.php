<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\SubUser;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('HasPermissions:2');
    }

    public function index()
    {

        $users = SubUser::paginate(5);
        return view('admin.pages.users.index', compact('users'));

    }

    public function add_user(Request $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt($request->password);
        $this->validate($request, [
            'name' => 'required|unique:visors',
            'email' => 'required|unique:visors',
            'password' => 'required|min:6'
        ],
            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'الإسم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.unique' => ' البريد الإلكترونى مسجل بالفعل',
                'password.required' => 'برجاء إدخال كلمه المرور',
                'password.min' => ' كلمه المرور لا تقل عن ٦ حروف',
            ]);
        if ($user = SubUser::create($input)) {
            return redirect()->back()->withSuccess('تم إضافه المستخدم بنجاح');
        }
        return redirect()->back()->withInput($request->all())->exceptInput($request->password);


    }

    public function edit_user(Request $request)
    {
        $input = $request->all();
        $user = SubUser::find($request->id);
        if (trim($request->password) == '') {

            $input = $request->except('password');

        } else {

            $input['password'] = bcrypt($request->password);

        }
        $this->validate($request, [
            'name' => 'required|unique:users,name,' . $user->id,
            'email' => 'required|unique:users,email,' . $user->id,
            'password' => 'nullable|min:6'
        ],
            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'الإسم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.unique' => ' البريد الإلكترونى مسجل بالفعل',
                'password.min' => ' كلمه المرور لا تقل عن ٦ حروف',
            ]);
        if ($user->update($input)) {
            return redirect()->back()->withSuccess('تم تعديل البيانات بنجاح');
        }
        return redirect()->back()->withInput($request->all())->exceptInput($request->password);


    }

    public function delete_user(Request $request)
    {
        $user = SubUser::find($request->id);
        $user->delete();
        session()->flash('deleted', 'تم حذف المستخدم بنجاح');
        return redirect()->back();


    }

    public function updateStatus(Request $request)
    {
        $user = SubUser::find($request->id);
        $user->is_active = $request->status;
        $user->save();
        return response()->json(["status" => "ok", 'user' => $user]);
    }
}
