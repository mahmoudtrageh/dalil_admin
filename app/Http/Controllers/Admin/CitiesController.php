<?php

namespace App\Http\Controllers\Admin;


use App\City;
use App\Http\Controllers\Controller;

use App\Mohafza;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    //

    public function index($id)
    {

        $cities = Mohafza::find($id)->cities;
        return view('admin.pages.cities.index', compact('cities', 'id'));

    }

    public function add_city(Request $request)
    {
        $this->validate($request, [
            'arabic_name' => 'required',
            'english_name' => 'required',
        ],
            [
                'arabic_name.required' => 'برجاء إدخال الاسم العربي',
                'english_name.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);

        if (City::create($request->all())) {
            return redirect()->back()->withSuccess('تم الاضافة');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function edit_city(Request $request)
    {
        $this->validate($request, [
            'arabic_name' => 'required',
            'english_name' => 'required',
        ],
            [
                'arabic_name.required' => 'برجاء إدخال الاسم العربي',
                'english_name.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);
        $checker = City::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess('تم التعديل');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function delete_city(Request $request)
    {
        City::find($request->id)->delete();
        return redirect()->back()->with('deleted', 'تم الحذف');

    }
}
