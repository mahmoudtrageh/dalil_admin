<?php

namespace App\Http\Controllers\Admin;


use App\Country;
use App\Http\Controllers\Controller;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SectionsController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('HasPermissions:5');
    }

    public function index()
    {

        $sections = Section::all();
        return view('admin.pages.sections.index', compact('sections'));

    }

    public function add_section(Request $request)
    {
        $this->validate($request, [
            'arabic_name' => 'required',
            'english_name' => 'required',
        ],
            [
                'arabic_name.required' => 'برجاء إدخال الاسم العربي',
                'english_name.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);

        if (Section::create($request->all())) {
            return redirect()->back()->withSuccess('تم الاضافة');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function edit_section(Request $request)
    {
        $this->validate($request, [
            'arabic_name' => 'required',
            'english_name' => 'required',
        ],
            [
                'arabic_name.required' => 'برجاء إدخال الاسم العربي',
                'english_name.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);
        $checker = Section::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess('تم التعديل');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function delete_section(Request $request)
    {
        Section::find($request->id)->delete();
        return redirect()->back()->with('deleted', 'تم الحذف');

    }

}