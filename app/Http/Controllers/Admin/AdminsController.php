<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class AdminsController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('HasPermissions:3');
    }


    public function index()
    {

//        $roles = Role::all();
//        $visors = User::all();

        $roles = Role::where('id', '!=', '3')->get();
        $visors = User::where('id', '!=', '2')->get();

        return view('admin.pages.supervisors.index', compact('visors', 'roles'));

    }

    public function add_visor(Request $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt($request->password);
        $this->validate($request, [
            'name' => 'required|unique:visors',
            'email' => 'required|unique:visors',
            'password' => 'required|min:6',
            'role_id' => 'required'
        ],
            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'الإسم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.unique' => ' البريد الإلكترونى مسجل بالفعل',
                'password.required' => 'برجاء إدخال كلمه المرور',
                'password.min' => ' كلمه المرور لا تقل عن ٦ حروف',
                'role_id.required' => ' برجاء إختيار صلاحيه واحده على الأقل',
            ]);
        if ($visor = User::create($input)) {
            $visor->roles()->attach($request->role_id);
            return redirect()->back()->withSuccess('تم إضافه المشرف بنجاح');
        }
        return redirect()->back()->withInput($request->all())->exceptInput($request->password);


    }

    public function edit_visor(Request $request)
    {
        $input = $request->all();
        $visor = User::find($request->id);
        if (trim($request->password) == '') {

            $input = $request->except('password');

        } else {

            $input['password'] = bcrypt($request->password);

        }
        $this->validate($request, [
            'name' => 'required|unique:visors,name,' . $visor->id,
            'email' => 'required|unique:visors,email,' . $visor->id,
            'password' => 'nullable|min:6',
            'role_id' => 'required'
        ],
            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'الإسم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.unique' => ' البريد الإلكترونى مسجل بالفعل',
                'password.min' => ' كلمه المرور لا تقل عن ٦ حروف',
                'role_id.required' => ' برجاء إختيار صلاحيه واحده على الأقل',
            ]);
        if ($visor->update($input)) {
            $visor->roles()->sync($request->role_id);
            return redirect()->back()->withSuccess('تم تعديل البيانات بنجاح');
        }
        return redirect()->back()->withInput($request->all())->exceptInput($request->password);


    }

    public function delete_visor(Request $request)
    {
        $visor = User::find($request->id);
        $visor->roles()->detach();
        $visor->delete();
        session()->flash('deleted', 'تم حذف المشرف بنجاح');
        return redirect()->back();


    }

    public function updateStatus(Request $request)
    {
        $user = User::find($request->id);
        $user->is_active = $request->status;
        $user->save();
        return response()->json(["status" => "ok", 'user' => $user]);
    }


}
