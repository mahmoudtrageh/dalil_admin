<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Company;
use App\Country;
use App\Http\Controllers\Controller;

use App\Mohafza;
use App\Section;
use App\SubUser;
use App\Tasnif;
use App\Zone;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('HasPermissions:1');
    }

    public function index()
    {
        $countries = count(Country::all());
        $cities = count(City::all());
        $mohafzas = count(Mohafza::all());
        $zones = count(Zone::all());
        $sections = count(Section::all());
        $tasnifs = count(Tasnif::all());
        $companies = count(Company::all());
        $users = count(SubUser::all());

        return view('admin.pages.index.index', compact('countries','cities','mohafzas','zones', 'sections', 'tasnifs',
            'companies', 'users'));
    }

}
