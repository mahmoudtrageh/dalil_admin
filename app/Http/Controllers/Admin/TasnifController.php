<?php

namespace App\Http\Controllers\Admin;


use App\City;
use App\Http\Controllers\Controller;

use App\Mohafza;
use App\Section;
use App\Tasnif;
use Illuminate\Http\Request;

class TasnifController extends Controller
{
    //

    public function index($id)
    {

        $tasnifs = Section::find($id)->tasnifs;
        return view('admin.pages.tasnif.index', compact('tasnifs', 'id'));

    }

    public function add_tasnif(Request $request)
    {
        $this->validate($request, [
            'arabic_name' => 'required',
            'english_name' => 'required',
        ],
            [
                'arabic_name.required' => 'برجاء إدخال الاسم العربي',
                'english_name.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);

        if (Tasnif::create($request->all())) {
            return redirect()->back()->withSuccess('تم الاضافة');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function edit_tasnif(Request $request)
    {
        $this->validate($request, [
            'arabic_name' => 'required',
            'english_name' => 'required',
        ],
            [
                'arabic_name.required' => 'برجاء إدخال الاسم العربي',
                'english_name.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);
        $checker = Tasnif::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess('تم التعديل');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function delete_tasnif(Request $request)
    {
        Tasnif::find($request->id)->delete();
        return redirect()->back()->with('deleted', 'تم الحذف');

    }
}