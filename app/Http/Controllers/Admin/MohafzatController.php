<?php

namespace App\Http\Controllers\Admin;


use App\Country;
use App\Http\Controllers\Controller;

use App\Mohafza;
use Illuminate\Http\Request;

class MohafzatController extends Controller
{
    //

    public function index($id)
    {

        $mohafzas = Country::find($id)->mohafzas;
        return view('admin.pages.mohafzas.index', compact('mohafzas', 'id'));

    }

    public function add_mohafza(Request $request)
    {
        $this->validate($request, [
            'arabic_name' => 'required',
            'english_name' => 'required',
        ],
            [
                'arabic_name.required' => 'برجاء إدخال الاسم العربي',
                'english_name.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);

//        dd($request->all());

        if (Mohafza::create($request->all())) {
            return redirect()->back()->withSuccess('تم الإضافه');
        }

        return redirect()->back()->withInput($request->all());

    }

    public function edit_mohafza(Request $request)
    {
        $this->validate($request, [
            'arabic_name' => 'required',
            'english_name' => 'required',
        ],
            [
                'arabic_name.required' => 'برجاء إدخال الاسم العربي',
                'english_name.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);
        $checker = Mohafza::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess('تم التعديل');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function delete_mohafza(Request $request)
    {
        Mohafza::find($request->id)->delete();
        return redirect()->back()->with('deleted', 'تم الحذف');

    }
}
