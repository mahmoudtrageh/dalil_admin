<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AuthController extends Controller
{
    //

    public function index()
    {
        return view('admin.pages.auth.index');

    }
    public function login(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'password' => 'required'
        ],
            [
                'name.required' => 'برجاء إدخال الإسم',
                'password.required' => 'برجاء إدخال كلمه المرور'
            ]);

        if (auth()->guard('admin')->attempt(['email' => $request->name, 'password' => $request->password])) {
            return redirect()->route('supervisors.index');
        } elseif (auth()->guard('admin')->attempt(['name' => $request->name, 'password' => $request->password])) {
            return redirect()->route('supervisors.index');
        }
        return redirect()->route('admin.get.login')->withErrors('برجاء التأكد من إسم المستخدم وكلمه المرور');
    }
    public function logout(){
        auth()->guard('admin')->logout();
        return redirect()->route('admin.get.login');
    }

}
