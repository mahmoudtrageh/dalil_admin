<?php

namespace App\Http\Controllers\Admin;


use App\City;
use App\Company;
use App\Country;
use App\Http\Controllers\Controller;
use App\Mohafza;
use App\Section;
use App\Tasnif;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use PHPUnit\Framework\Constraint\Count;

class CompaniesController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('HasPermissions:6');
    }

    public function index()
    {


        $companies = Company::all();
        $countries = Country::all();
        $mohafzas = Mohafza::all();
        $cities = City::all();
        $zones = Zone::all();
        $sections = Section::all();
        $tasnifs = Tasnif::all();

        return view('admin.pages.companies.index', compact('companies', 'countries', 'mohafzas', 'cities', 'zones', 'sections', 'tasnifs'));

    }

    public function add_company(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required',
            'country_id' => 'required',
            'mohafza_id' => 'required',
            'city_id' => 'required',
            'zone_id' => 'required',
            'section_id' => 'required',
            'tasnif_id' => 'required',
        ],
            [
                'name.required' => 'برجاء إدخال الاسم',
                'code.required' => 'برجاء ادخال الكود',
                'country_id.required' => 'برجاء اختيار الدولة',
                'mohafza_id.required' => 'برجاء اختيار المحافظة',
                'city_id.required' => 'برجاء اختيار المدينة',
                'zone_id.required' => 'برجاء اختيار المنطقة',
                'section_id.required' => 'برجاء اختيار القطاع',
                'tasnif_id.required' => 'برجاء اختيار التصنيف',
            ]);

        if (Company::create($request->all())){
            return redirect()->back()->withSuccess('تم الاضافة');

        }
        return redirect()->back()->withInput($request->all());

    }

    public function edit_company(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'code' => 'required',
            'country' => 'required',
            'mohafza' => 'required',
            'city' => 'required',
            'zone' => 'required',
            'section' => 'required',
            'tasnif' => 'required',
        ],
            [
                'name.required' => 'برجاء إدخال الاسم',
                'code.required' => 'برجاء ادخال الكود',
                'country.required' => 'برجاء اختيار الدولة',
                'mohafza.required' => 'برجاء اختيار المحافظة',
                'city.required' => 'برجاء اختيار المدينة',
                'zone.required' => 'برجاء اختيار المنطقة',
                'section.required' => 'برجاء اختيار القطاع',
                'tasnif.required' => 'برجاء اختيار التصنيف',
            ]);
        $checker = Company::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess('تم التعديل');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function delete_company(Request $request)
    {
        Company::find($request->id)->delete();
        return redirect()->back()->with('deleted', 'تم الحذف');

    }

    public function filter(Request $request)

    {
        $countries = Country::all();

        $country = $request->country;

        $companies = Company::all();

        if ($country)

            $companies = $companies->where('country_id','=', $country);




        $mohafzas = Mohafza::all();

            $mohafza = $request->mohafza;

        if($mohafza)

            $companies = $companies->where('mohafza_id','=', $mohafza);


        $cities = City::all();

        $city = $request->city;

        if($city)

            $companies = $companies->where('city_id','=', $city);

        $zones = Zone::all();

        $zone = $request->zone;

        if($zone)

            $companies = $companies->where('zone_id','=', $zone);

        $sections = Section::all();

        $section = $request->section;

        if($section)

            $companies = $companies->where('section_id','=', $section);

        $tasnifs = Tasnif::all();

        $tasnif = $request->tasnif;

        if($tasnif)

            $companies = $companies->where('tasnif_id','=', $tasnif);

        $code = $request->code;

        if($code)

            $companies = $companies->where('code','=', $code);

        $name = $request->name;

        if($name)

            $companies = $companies->where('name','=', $name);


        return view('admin.pages.companies.index', compact('companies', 'filter', 'countries', 'mohafzas', 'cities', 'zones', 'sections', 'tasnifs'));


    }

    public function getMohafza(Request $request)
    {
        $user = Country::find($request->id);


        $mohafzat = Mohafza::where('country_id', $user->id)->get();

        return response()->json(["status" => "ok", 'user' => $mohafzat]);
    }

    public function getCity(Request $request)
    {
        $user = Mohafza::find($request->id);


        $cities = City::where('mohafza_id', $user->id)->get();

        return response()->json(["status" => "ok", 'user' => $cities]);
    }

    public function getZone(Request $request)
    {
        $user = City::find($request->id);


        $zones = Zone::where('city_id', $user->id)->get();

        return response()->json(["status" => "ok", 'user' => $zones]);
    }

    public function getTasnif(Request $request)
    {
        $user = Section::find($request->id);


        $tasnifs = Tasnif::where('section_id', $user->id)->get();

        return response()->json(["status" => "ok", 'user' => $tasnifs]);
    }



}