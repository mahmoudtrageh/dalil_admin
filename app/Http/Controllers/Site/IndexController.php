<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use App\City;
use App\Country;
use App\Mohafza;
use App\Section;
use App\Tasnif;
use App\Zone;

class IndexController extends Controller
{
    //

    public function index()
    {

        $companies = Company::all();
        $countries = Country::all();
        $mohafzas = Mohafza::all();
        $cities = City::all();
        $zones = Zone::all();
        $sections = Section::all();
        $tasnifs = Tasnif::all();


        return view('site.pages.index.index', compact('companies', 'countries', 'mohafzas', 'cities',
            'zones', 'sections', 'tasnifs'));
    }

}
