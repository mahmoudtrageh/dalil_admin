<?php

namespace App\Http\Controllers\Site;

use App\SubUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;


class ProfileController extends Controller
{
    public function index()
    {
        return view('site.pages.profile.index');
    }

    public function update_profile(Request $request)
    {
        $checker = SubUser::find(auth()->user()->id);
        $input = $request->all();
        $this->validate($request, [

            'name' => 'required|unique:users,name,' . $checker->id,
            'email' => 'required|unique:users,email,' . $checker->id,
            'ph_number' => 'required',

        ],

            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'الإسم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.unique' => 'البريد الإلكترونى مسجل بالفعل',
                'ph_number.required' => 'برجاء إدخال رقم الموبايل',

            ]);

        if ($checker->update($input)) {
            session()->flash('success', 'تم تعديل البيانات بنجاح');
            return redirect()->back();
        }

        return redirect()->back()->withInput($request->all());

    }

    public function update_password(Request $request)
    {
        $this->validate($request, [

            'old_password' => 'required',
            'new_password' => 'required|min:6|max:15',
            'new_password_confirmation' => 'required|same:new_password',

        ],

            [
                'old_password.required' => 'برجاء إدخال كلمه المرور القديمه',
                'new_password.required' => 'برجاء إدخال كلمه المرور الجديده',
                'new_password.min' => ' كلمه المرور الجديده لا تقل عن 6 حروف',
                'new_password.max' => ' كلمه المرور الجديده لا تزيد عن 15 حروف',
                'new_password_confirmation.required' => 'برجاء إدخال تأكيد كلمه المرور الجديده',
                'new_password_confirmation.same' => 'تأكيد كلمه المرور الجديده لا تتطابق',

            ]);

        $user = SubUser::find(auth()->user()->id);
        $input = $request->all();
        $input['new_password'] = bcrypt($request->new_password);
        if (Hash::check($request->old_password, $user->password)) {
            $user->update(['password' => $input['new_password']]);
            session()->flash('success', 'تم تحديث كلمه المرور بنجاح');
            return redirect()->back();
        }
        session()->flash('error', 'كلمه المرور القديمه غير صحيحه');
        return redirect()->back();

    }

}