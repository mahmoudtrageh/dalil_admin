<?php

namespace App\Http\Controllers\Site;;

use App\Events\NewInquiryEvent;
use App\Messages;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    public function index()
    {
        return view('site.pages.contact-us.index');
    }

    public function contact_us(Request $request)
    {
        $this->validate($request, [

            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'message' => 'required',

        ],

            [
                'name.required' => 'برجاء إدخال الإسم',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.email' => 'برجاء إدخال البريد الإلكترونى بصيغه صحيحه (Example@example.com)',
                'phone.required' => 'برجاء إدخال رقم الهاتف',
                'message.required' => 'برجاء إدخال الرساله',
            ]);

        if ($message = Messages::create($request->all())) {

            event(new NewInquiryEvent());
            session()->flash('success', 'تم إرسال الرساله بنجاح وسيتم التواصل معك فى أسرع وقت');
            return redirect()->back();

        }
        return redirect()->back()->withInput($request->all());
    }
}
