<?php

namespace App\Http\Controllers\Site\Auth;

use App\Http\Controllers\Controller;

use App\Events\NewUserEvent;
use App\Events\UserRegistered;
use App\Mail\ResetPassword;
use App\Notifications\NewUser;
use App\User;
use Illuminate\Http\Request;
use App\SubUser;
use App\Code;
use Carbon\Carbon;
use Auth;

class AuthController extends Controller
{
    //

    public function get_register()

    {
        return view('site.pages.auth.register');
    }


    public function register(Request $request)

    {

        $this->validate($request, [

            'name' => 'required|unique:users,name',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6|max:15',
            'password_confirmation' => 'required|same:password'
        ],
            [
                'name.required' => 'برجاء إدخال الإسم',
                'name.unique' => 'الإسم مسجل بالفعل',
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
                'email.unique' => 'البريد الإلكترونى مسجل بالفعل',
                'password.required' => 'برجاء إدخال كلمه المرور',
                'password_confirmation.required' => 'برجاء إدخال تأكيد كلمه المرور',
                'password_confirmation.same' => 'كلمه المرور وتأكيدها لا تتطابق',
                'password.min' => 'كلمه المرور لا تقل عن 6 حروف',
                'password.max' => 'كلمه المرور لا تزيد عن 15 حوف',
            ]);

        $input = $request->all();
        $input['password'] = bcrypt($request->password);
        if ($user = SubUser::create($input)) {
            {
                event(new NewUserEvent('user'));
                auth()->login($user);
                return redirect()->intended(route('site.get.login'))->withErrors('تم تسجيل الحساب وبانتظار الموافقة من الأدمن');
            }
        }

        return redirect()->back()->withInput($request->all())->exceptInput($request->password);

    }


    public function get_login()
    {
        return view('site.pages.auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ], [
            'email.required' => 'برجاء إدخال إسم المتسخدم أو  البريد الإلكتروني',
            'password.required' => 'برجاء إدخال كلمه المرور',
        ]);
        if ($checker = SubUser::where('email', $request->email)->first()) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember_me)) {
                if ($checker->is_active ==1) {
                    return redirect()->intended(route('site.index'));
                } else {
                    return redirect()->route('site.get.login')->withErrors('برجاء انتظار التأكيد من الأدمن');
                }
            }
        }
//        session()->flash('login_error', 'برجاء التأكد من إسم المستخدم وكلمه المرور');
        return redirect()->route('site.get.login')->withErrors('برجاء التأكد من إسم المستخدم وكلمه المرور');
    }

    public function logout()
    {

        Auth::logout();
        return redirect()->route('site.get.login');

    }

    public function forget()

    {
        return view('site.pages.auth.reset-password');
    }

    public function check(Request $request)

    {

        $this->validate($request, [

            'email' => 'required',
        ],

            [
                'email.required' => 'برجاء إدخال البريد الإلكترونى',
            ]);
        if ($user = SubUser::where('email', $request->email)->where('is_active', 1)->first()) {
            $email = $user->email;
            $code = str_random(5);
            $contact_route=route('site.contact.index');
            if ($user->code) {
                $user->code()->update(['code' => $code, 'updated_at' => Carbon::now()]);
            }
            else
            {
                $user->code()->create(['code' => $code, 'updated_at' => Carbon::now()]);
            }
            \Mail::to($user)->send(new ResetPassword($code,$contact_route));
            return view('site.pages.auth.confirm-code', compact('email'));
        }
        session()->flash('error', 'البريد الإلكترونى غير مسجل');
        return redirect()->back();

    }

    public function code_confirmation(Request $request)

    {
//        dd($request->all());
        $email = ($request->email);

        if ($request->code == null) {
            session()->flash('error', 'برجاء إدخال الكود');
            return view('site.pages.auth.confirm-code', compact('email'));
        }

        if (!$request->email || !SubUser::where('email', $request->email)->first() || !SubUser::where('email', $request->email)->first()->code) {
            session()->flash('error', 'حدث خطأ , برجاء المحاوله ثانيه');
            return redirect()->route('site-forget');
        } else {

            $user = SubUser::where('email', $request->email)->first();


            if ($request->code != $user->code->code) {
                session()->flash('error', 'الكود غير صحيح');
                return view('site.pages.auth.confirm-code', compact('email'));
            }

            if ($request->code == $user->code->code) {
                if ($user->code->updated_at->diffInHours() < 2) {
                    $user->code->update(['code' => str_random(5), 'updated_at' => Carbon::now()]);
                    return view('site.pages.auth.new-password', compact('email'));
                }
            }
            session()->flash('error', 'إنتهت صلاحيه الكود , برجاء المحاوله ثانيه');
            return redirect()->route('site-forget');

        }

    }

    public function pass_change(Request $request)

    {
        $email = $request->email;
        $v = validator($request->all(), [
            'password' => 'required|min:6|max:15',
            'password_confirmation' => 'required|same:password',
        ],

            [
                'password.required' => 'برجاء إدخال كلمه المرور',
                'password_confirmation.required' => 'برجاء إدخال تأكيد كلمه المرور',
                'password_confirmation.same' => 'كلمه المرور وتأكيدها لا تتطابق',
                'password.min' => 'كلمه المرور لا تقل عن 6 حروف',
                'password.max' => 'كلمه المرور لا تزيد عن 15 حوف',

            ]);

        if ($v->fails()) {
            return view('site.pages.auth.new-password', compact('email'))->withErrors($v);
        }

        if ($user = SubUser::where('email', $request->email)->first()) {

            $user->update(['password' => bcrypt($request->password)]);
            Auth::login($user);
            return redirect()->intended(route('site.index'));

        }
        session()->flash('error', 'حدث خطأ , برجاء المحاوله ثانيه');
        return redirect()->route('site-forget');
    }



}
