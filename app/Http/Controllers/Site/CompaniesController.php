<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\City;
use App\Company;
use App\Country;
use App\Http\Controllers\Controller;
use App\Mohafza;
use App\Section;
use App\Tasnif;
use App\Zone;

class CompaniesController extends Controller
{
    public function index()
    {
        $companies = Company::all();
        $countries = Country::all();
        $mohafzas = Mohafza::all();
        $cities = City::all();
        $zones = Zone::all();
        $sections = Section::all();
        $tasnifs = Tasnif::all();
        return view('admin.pages.companies.index', compact('companies', 'countries', 'mohafzas', 'cities', 'zones', 'sections', 'tasnifs'));
    }

//    public function edit_company(Request $request)
//    {
//        $this->validate($request, [
//            'name' => 'required',
//            'code' => 'required',
//            'country' => 'required',
//            'mohafza' => 'required',
//            'city' => 'required',
//            'zone' => 'required',
//            'section' => 'required',
//            'tasnif' => 'required',
//            'phone_number' => 'required',
//            'res_name' => 'required',
//            'mobile_number' => 'required',
//            'whatsapp_number' => 'required',
//            'email' => 'required',
//
//        ],
//            [
//                'name.required' => 'برجاء إدخال الاسم',
//                'code.required' => 'برجاء ادخال الكود',
//                'country.required' => 'برجاء اختيار الدولة',
//                'mohafza.required' => 'برجاء اختيار المحافظة',
//                'city.required' => 'برجاء اختيار المدينة',
//                'zone.required' => 'برجاء اختيار المنطقة',
//                'section.required' => 'برجاء اختيار القطاع',
//                'tasnif.required' => 'برجاء اختيار التصنيف',
//                'phone_number.required' => 'برجاء ادخال رقم التليفون',
//                'res_name.required' => 'برجاء ادخال رقم المسؤول',
//                'mobile_number.required' => 'برجاء ادخال رقم الموبايل',
//                'whatsapp_number.required' => 'برجاء ادخال رقم الواتس',
//                'email.required' => 'برجاء ادخال البريد الالكتروني',
//
//            ]);
//        $checker = Company::find($request->id);
//        if ($checker->update($request->all())) {
//            return redirect()->back()->withSuccess('تم التعديل');
//        }
//        return redirect()->back()->withInput($request->all());
//
//    }
//
//    public function delete_company(Request $request)
//    {
//        Company::find($request->id)->delete();
//        return redirect()->back()->with('deleted', 'تم الحذف');
//
//    }

    public function filter(Request $request)

    {
        $countries = Country::all();
        $country = $request->country;
        $companies = Company::all();

        if ($country)
            $companies = $companies->where('country_id','=', $country);
            $mohafzas = Mohafza::all();
            $mohafza = $request->mohafza;

        if($mohafza)
            $companies = $companies->where('mohafza_id','=', $mohafza);
            $cities = City::all();
            $city = $request->city;

        if($city)
            $companies = $companies->where('city_id','=', $city);
            $zones = Zone::all();
            $zone = $request->zone;

        if($zone)
            $companies = $companies->where('zone_id','=', $zone);
            $sections = Section::all();
            $section = $request->section;

        if($section)
            $companies = $companies->where('section_id','=', $section);
            $tasnifs = Tasnif::all();
            $tasnif = $request->tasnif;

        if($tasnif)
            $companies = $companies->where('tasnif_id','=', $tasnif);
            $code = $request->code;

        if($code)
            $companies = $companies->where('code','=', $code);
            $name = $request->name;

        if($name)
            $companies = $companies->where('name','=', $name);
        return view('site.pages.index.index', compact('companies', 'filter', 'countries', 'mohafzas', 'cities', 'zones', 'sections', 'tasnifs'));


    }

    public function getMohafza(Request $request)
    {
        $user = Country::find($request->id);
        $mohafzat = Mohafza::where('country_id', $user->id)->get();
        return response()->json(["status" => "ok", 'user' => $mohafzat]);
    }

    public function getCity(Request $request)
    {
        $user = Mohafza::find($request->id);
        $cities = City::where('mohafza_id', $user->id)->get();
        return response()->json(["status" => "ok", 'user' => $cities]);
    }

    public function getZone(Request $request)
    {
        $user = City::find($request->id);
        $zones = Zone::where('city_id', $user->id)->get();
        return response()->json(["status" => "ok", 'user' => $zones]);
    }

    public function getTasnif(Request $request)
    {
        $user = Section::find($request->id);
        $tasnifs = Tasnif::where('section_id', $user->id)->get();
        return response()->json(["status" => "ok", 'user' => $tasnifs]);
    }
}
