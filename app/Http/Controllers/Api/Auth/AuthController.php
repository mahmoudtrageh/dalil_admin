<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\SubUser;
use Auth;

class AuthController extends Controller
{
    //

    public function get_login()
    {
        return view('site.pages.auth.login');
    }

    public function login(Request $request)
    {
        $v = Validator($request->all(),
            [
            'email' => 'required',
            'password' => 'required',
            ],

            [
            'email.required' => 'please enter email or username',
            'password.required' => 'please enter password',
        ]
        );

        if ($v->fails()) {
            $error = $v->errors()->first();
            if($error == 'please enter email or username')
                $error = 'برجاء إدخال إسم المتسخدم أو  البريد الإلكتروني';

            else if	($error == 'please enter password')
                $error = 'من فضلك ادخل كلمة المرور';

            else
                $error = 'تأكد من بياناتك فضلاً';


                $Result = [
                    'status' =>
                        ['type' => '0', 'title' => ['en' => $v->errors()->first(), 'ar' => $error]],
                ];
                return response()->json($Result);
        }


        if ($checker = SubUser::where('email', $request->email)->first()) {
            if (!$checker->is_active) {
                $Result = [
                    'status' =>
                        ['type' => '0', 'title' => 'User Not Active'],
                ];
                return response()->json($Result);
            }

            $checker->update(['api_token' => str_random(60)]);
            $Result = [
                'status' =>
                    ['type' => '1', 'title' => 'User Logged In Successfully'],
                'data' => $checker
            ];
            return response()->json($Result);
        }
        $Result = [
            'status' =>
                ['type' => '0', 'title' => 'برجاء التأكد من إسم المستخدم و كلمه المرور'],
        ];
        return response()->json($Result);
    }

    public function logout()
    {

        Auth::logout();
        return redirect()->route('site.get.login');

    }


}
