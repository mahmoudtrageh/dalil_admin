<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests\CityEditRequest;
use App\Http\Requests\CityRequest;
use App\Mohafza;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
        $cities = Mohafza::find($id)->cities;
        return view('admin.cities.index', compact('cities', 'id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        //

        $input = $request->all();

        City::create($input);

        Session::flash('add_city', 'لقد تم الاضافة بنجاح');


        return redirect()->back();

    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CityEditRequest $request, $id)
    {
        //

        $city = City::find($id);



        $input = $request->all();


        $city->update($input);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $city = Mohafza::findOrFail($id);

        $city->delete();

        // message of delete

        Session::flash('delete_city', 'لقد تم الحذف بنجاح');

        return redirect()->back();
    }
}
