<?php

namespace App\Http\Controllers;

use App\Http\Requests\MohafzaRequest;
use App\Mohafza;
use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PHPUnit\Framework\Constraint\Count;

class MohafzasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //

        $mohafzas = Country::find($id)->mohafzas;
        return view('admin.mohafzas.index', compact('mohafzas', 'id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MohafzaRequest $request)
    {
        //





        $input = $request->all();

        Mohafza::create($input);

//        $mohafza->countries()->attach($request->country_id);

        Session::flash('add_mohafza', 'لقد تم الاضافة بنجاح');


        return redirect()->back();



//
//
//        if (Country::find($request->id)->cities()->create($request->all())) {
//            return redirect()->back()->withSuccess('تم الإضافه');
//        }
//        return redirect()->back()->withInput($request->all());


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $mohafza = Mohafza::find($id);



        $input = $request->all();


//        $visor->roles()->sync($request->role_id);

        $mohafza->update($input);

//        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $mohafza = Mohafza::findOrFail($id);

//        $visor->roles()->detach();

        $mohafza->delete();

        // message of delete

        Session::flash('delete_mohafza', 'لقد تم الحذف بنجاح');

        return redirect()->back();
    }
}
