<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;
    public $code;
    public $contact_route;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code, $contact_route)
    {
        $this->code = $code;
        $this->contact_route = $contact_route;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('site.pages.auth.email.reset-password');
//            ->from('geo.mahmoudtaha@gmail.com');

    }
}
