<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{

    public $table = 'codes';

    protected $fillable = [

        'user_id',
        'code',
        'updated_at',
    ];

    public function user(){
        return $this->belongsTo('App\SubUser');
    }

}
