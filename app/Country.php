<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //

    protected $fillable = [
        'arabic_name', 'english_name', 'id',
    ];

    protected $table = 'countries';


    // many to

    public function mohafzas(){
        return $this->hasMany('App\Mohafza');
    }
    public function companies(){
        return $this->hasMany('App\Company');
    }
}
