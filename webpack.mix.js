const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');

mix.styles([
        'public/app-assets/css/bootstrap-extended.css',
    'public/app-assets/css/colors.css',
    'public/app-assets/css/components.css',
    'public/app-assets/css-rtl/custom-rtl.css',
    'public/app-assets/fonts/simple-line-icons/style.css',
    'public/app-assets/css-rtl/core/menu/menu-types/vertical-menu.css',
    'public/app-assets/css-rtl/core/colors/palette-gradient.css',
    'public/app-assets/vendors/css/cryptocoins/cryptocoins.css',
    'public/assets/css/style-rtl.css',
    'public/app-assets/css-rtl/plugins/animate/animate.css',
    'public/app-assets/vendors/css/forms/selects/selectize.css',
    'public/app-assets/vendors/css/forms/selects/selectize.default.css',
    'public/app-assets/vendors/css/tables/datatable/datatables.min.css',
    'public/app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css',
    'public/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css',
    'public/app-assets/vendors/css/tables/datatable/select.dataTables.min.css',
    'public/app-assets/vendors/css/tables/extensions/fixedColumns.dataTables.min.css',
    'public/app-assets/vendors/css/pickers/daterange/daterangepicker.css',
    'public/app-assets/css-rtl/plugins/forms/wizard.css',
    'public/app-assets/css-rtl/plugins/pickers/daterange/daterange.css',
    'public/app-assets/css-rtl/plugins/forms/selectize/selectize.css',
], 'public/css/admin.css');

mix.scripts([
    'public/app-assets/vendors/js/vendors.min.js',
    'public/app-assets/vendors/js/charts/chart.min.js',
    'public/app-assets/vendors/js/charts/echarts/echarts.js',
    'public/app-assets/js/core/app-menu.js',
    'public/app-assets/js/core/app.js',
    'public/app-assets/js/scripts/customizer.js',
    'public/app-assets/js/scripts/pages/dashboard-crypto.js',
    'public/app-assets/vendors/js/extensions/jquery.steps.min.js',
    'public/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js',
    'public/app-assets/vendors/js/pickers/daterange/daterangepicker.js',
    'public/app-assets/vendors/js/forms/validation/jquery.validate.min.js',
    'public/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js',
    'public/app-assets/js/scripts/ui/jquery-ui/dialog-tooltip.js',
    'public/app-assets/vendors/js/forms/select/selectize.min.js',
    'public/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js',
    'public/app-assets/js/scripts/forms/wizard-steps.js',
    'public/app-assets/vendors/js/tables/datatable/datatables.min.js',
    'public/app-assets/vendors/js/tables/datatable/dataTables.fixedColumns.min.js',
    'public/app-assets/vendors/js/tables/buttons.colVis.min.js',
    'public/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js',
    'public/app-assets/vendors/js/tables/datatable/dataTables.select.min.js',
    'public/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js',
    'public/app-assets/js/scripts/tables/datatables-extensions/datatable-fixed-column.js',
    'public/app-assets/js/scripts/forms/select/form-selectize.js',

], 'public/js/admin.js');

// site

mix.styles([
    'public/site/css/bootstrap.min.css',
    'public/site/css/bootstarpRTl.css',
    'public/site/css/font-awesome.min.css',
        'public/site/https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css',
        'public/site/https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css',
        'public/site/css/main.css',
],'public/css/site.css');

mix.scripts([
    'public/site/js/jquery-3.3.1.min.js',
    'public/site/js/popper.min.js',
    'public/site/js/bootstrap.min.js',
    'public/site/https://code.jquery.com/jquery-3.3.1.js',
    'public/site/https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
    'public/site/https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js',
    'public/site/js/main.js',
],'public/js/site.js');




