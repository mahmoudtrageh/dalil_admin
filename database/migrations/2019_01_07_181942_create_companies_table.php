<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->integer('code');
            $table->string('country');
            $table->string('mohafza');
            $table->string('city');
            $table->string('zone');
            $table->string('section');
            $table->string('tasnif');
            $table->integer('phone_number')->nullable();
            $table->string('res_name')->nullable();
            $table->integer('mobile_number')->nullable();
            $table->integer('whatsapp_number')->nullable();
            $table->string('email')->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
