<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use App\Mohafza;



Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

    // Auth Routes

    Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {

        Route::get('login', 'AuthController@index')->name('admin.get.login');
        Route::post('logging', 'AuthController@login')->name('admin.login');
        Route::get('logout', 'AuthController@logout')->name('admin.logout');

    });


    Route::group(['middleware'=>'Admin'], function (){

        /* Admin Index Route */
        Route::get('index', 'IndexController@index')->name('admin.index');


        // Admins Routes
    Route::group(['prefix' => 'supervisors'], function () {

        Route::get('', 'AdminsController@index')->name('supervisors.index');
        Route::post('add-visor', 'AdminsController@add_visor')->name('add.visor');
        Route::post('edit-visor', 'AdminsController@edit_visor')->name('edit.visor');
        Route::post('delete-visor', 'AdminsController@delete_visor')->name('delete.visor');
        Route::post('/update-status', 'AdminsController@updateStatus')->name('update.status.visor');
    });

    // Users Routes
    Route::group(['prefix' => 'users'], function () {

        Route::get('', 'UsersController@index')->name('users.index');
        Route::post('add-user', 'UsersController@add_user')->name('add.user');
        Route::post('edit-user', 'UsersController@edit_user')->name('edit.user');
        Route::post('delete-user', 'UsersController@delete_user')->name('delete.user');
        Route::post('/update-status', 'UsersController@updateStatus')->name('update.status.user');
    });

// Countries Routes
    Route::group(['prefix' => 'countries'], function () {

        Route::get('', 'CountriesController@index')->name('countries.index');
        Route::post('add-country', 'CountriesController@add_country')->name('add.country');
        Route::post('edit-country', 'CountriesController@edit_country')->name('edit.country');
        Route::post('delete-country', 'CountriesController@delete_country')->name('delete.country');

    });

    Route::group(['prefix' => 'mohafzas'], function () {

        Route::get('/{id}', 'MohafzatController@index')->name('mohafzas.index');
        Route::post('add-mohafza', 'MohafzatController@add_mohafza')->name('add.mohafza');
        Route::post('edit-mohafza', 'MohafzatController@edit_mohafza')->name('edit.mohafza');
        Route::post('delete-mohafza', 'MohafzatController@delete_mohafza')->name('delete.mohafza');
    });

    Route::group(['prefix' => 'cities'], function () {

        Route::get('/{id}', 'CitiesController@index')->name('cities.index');
        Route::post('add-city', 'CitiesController@add_city')->name('add.city');
        Route::post('edit-city', 'CitiesController@edit_city')->name('edit.city');
        Route::post('delete-city', 'CitiesController@delete_city')->name('delete.city');
    });

    Route::group(['prefix' => 'zones'], function () {

        Route::get('/{id}', 'ZonesController@index')->name('zones.index');
        Route::post('add-zone', 'ZonesController@add_zone')->name('add.zone');
        Route::post('edit-zone', 'ZonesController@edit_zone')->name('edit.zone');
        Route::post('delete-zone', 'ZonesController@delete_zone')->name('delete.zone');
    });

        Route::group(['prefix' => 'sections'], function () {

            Route::get('', 'SectionsController@index')->name('sections.index');
            Route::post('add-section', 'SectionsController@add_section')->name('add.section');
            Route::post('edit-section', 'SectionsController@edit_section')->name('edit.section');
            Route::post('delete-section', 'SectionsController@delete_section')->name('delete.section');
        });

        Route::group(['prefix' => 'tasnif'], function () {

            Route::get('/{id}', 'TasnifController@index')->name('tasnif.index');
            Route::post('add-tasnif', 'TasnifController@add_tasnif')->name('add.tasnif');
            Route::post('edit-tasnif', 'TasnifController@edit_tasnif')->name('edit.tasnif');
            Route::post('delete-tasnif', 'TasnifController@delete_tasnif')->name('delete.tasnif');
        });

        Route::group(['prefix' => 'companies'], function () {

            Route::get('', 'CompaniesController@index')->name('companies.index');
            Route::post('add-company', 'CompaniesController@add_company')->name('add.company');
            Route::post('edit-company', 'CompaniesController@edit_company')->name('edit.company');
            Route::post('delete-company', 'CompaniesController@delete_company')->name('delete.company');
            Route::get('filter','CompaniesController@filter')->name('company.filter');
            Route::post('/get-mohafza', 'CompaniesController@getMohafza')->name('admin.get.mohafza');
            Route::post('/get-city', 'CompaniesController@getCity')->name('admin.get.city');
            Route::post('/get-zone', 'CompaniesController@getZone')->name('admin.get.zone');
            Route::post('/get-tasnif', 'CompaniesController@getTasnif')->name('admin.get.tasnif');
        });



        // Profile Routes
        Route::group(['prefix' => 'profile'], function () {

            Route::get('', 'ProfileController@index')->name('profile.index');
            Route::post('profile-update', 'ProfileController@update_profile')->name('update.profile.admin');
            Route::get('change-password', 'ProfileController@change_password')->name('profile.change.password');
            Route::post('profile-password-update', 'ProfileController@update_profile_password')->name('update.profile.password.admin');
        });

        // Notifications  Routes
        Route::group(['prefix' => 'notifications'], function () {

            // New Users Notifications

            Route::post('get-notifications', 'NotificationsController@get_notifications')->name('get.notifications');
            Route::post('read', 'NotificationsController@read')->name('read.notifications');

//            // New Inquiry Notifications
//
//            Route::post('get-inquiry-notifications', 'NotificationsController@get_inquiry_notifications')->name('get.inquiry.notifications');
//            Route::post('read-inquiry', 'NotificationsController@read_inquiry')->name('read.inquiry.notifications');
//

        });





    }); // end of middleware


}); // end of Admin route

Route::group(['namespace' => 'Site'], function () {

    /* Site login , logout  and Reset routes*/

    Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function () {

        Route::get('/register', 'AuthController@get_register')->name('site.get.register');
        Route::post('/registered', 'AuthController@register')->name('site.register');
        Route::get('/login', 'AuthController@get_login')->name('site.get.login');
        Route::post('/check', 'AuthController@login')->name('site-login-check');
        Route::get('/logout', 'AuthController@logout')->name('site.logout');
        Route::get('/forget', 'AuthController@forget')->name('site-forget');
        Route::post('/check-email', 'AuthController@check')->name('site-check');
        Route::get('/code', 'AuthController@code')->name('site.code');
        Route::post('/code-confirmation', 'AuthController@code_confirmation')->name('code.confirmation');
        Route::post('/pass-change', 'AuthController@pass_change')->name('site.pass.change');

    });

        // index

    Route::get('index', 'IndexController@index')->name('site.index');

        Route::get('filter','CompaniesController@filter')->name('site.company.filter');
        Route::post('/get-mohafza', 'CompaniesController@getMohafza')->name('site.get.mohafza');
        Route::post('/get-city', 'CompaniesController@getCity')->name('site.get.city');
        Route::post('/get-zone', 'CompaniesController@getZone')->name('site.get.zone');
        Route::post('/get-tasnif', 'CompaniesController@getTasnif')->name('site.get.tasnif');

    /* Contact Us Routes */

    Route::group(['prefix' => 'contact-us'], function () {

        Route::get('', 'ContactUsController@index')->name('site.contact.index');
        Route::post('contact-us', 'ContactUsController@contact_us')->name('site.contact.us');

    });

    /* Profile Routes */
    Route::group(['prefix' => 'profile'], function () {

        Route::get('', 'ProfileController@index')->name('site.profile.index');
        Route::post('update-profile', 'ProfileController@update_profile')->name('site.update.profile');
        Route::post('update-password', 'ProfileController@update_password')->name('site.update.password');

    });



}); // end of site route
